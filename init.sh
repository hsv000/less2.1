#!/bin/bash

network_name=DjangoBlog
volume_name=less11_django_static

# create network&volume
docker network inspect $network_name >/dev/null 2>&1 || docker network create $network_name
docker volume inspect $volume_name  >/dev/null 2>&1 || docker volume create less11_django_static

# init django
# docker exec -it less11_app sh -c "python manage.py makemigrations && python manage.py migrate && python manage.py collectstatic"