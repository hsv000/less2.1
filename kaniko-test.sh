docker run -ti --rm \
    -v "$PWD/app":/workspace \
    -v ~/.docker/config.json:/kaniko/.docker/config.json:ro \
    gcr.io/kaniko-project/executor:debug \
    --cache \
    --dockerfile=Dockerfile \
    --destination=registry.gitlab.com/hsv000/less2.1/app_dev-kaniko1